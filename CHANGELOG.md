# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## 1.1.0 (2020-04-23)


### Features

* **init:** initial project set up ([84c965f](https://gitlab.com/sftwr-prjct-dev/sftwr/npm-pkg-eg/commit/84c965fc141f5f37cd1a4acf4de5271af4cef020))
